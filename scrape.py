import re
from urllib.error import URLError
from bs4 import BeautifulSoup as soup
from urllib.request import urlopen as urlReq
import datetime
from openpyxl import load_workbook
from os import startfile


def change_workbook(new_workbook):
    global book
    book = new_workbook


# end of change_workbook

def change_Recent_data(data):
    global Recent_data
    Recent_data = data


# end of change_Recent_data

# get index of next available row from file
def get_next_row(sheet):
    global book
    nextrow = 2
    ws = book.worksheets[sheet]

    while ws.cell(nextrow, 1).value is not None:
        nextrow += 1
    return nextrow


# end of get_next_row


# Generalized method to insert a row on the specified excel sheet
def insert_row(rowData, sheet, row):
    global book
    ws = book.worksheets[sheet]
    col = 1
    for cellValue in rowData:
        ws.cell(row, col).value = cellValue
        col += 1


# end of insert_row


# clear specified range of rows and columns from sheet
def delete_range(sheet, start_row, end_row, num_col):
    if start_row > end_row:
        return
    global book
    ws = book.worksheets[sheet]
    for row in range(start_row, end_row + 1):
        for col in range(1, num_col + 1):
            ws.cell(row, col).value = ''


# end delete_range


# gets the unique values for the results
def get_uniques():
    global book
    s = set()
    for row in Recent_data:
        s.add(row[0])
    row = 2
    for bucket in s:  # adds each unique item in PriceHistory to next row in Results
        f1 = "=OFFSET(INDEX(Recent!A:A,MATCH(A" + str(row) + ",Recent!A:A,0)),0,1)"
        f2 = "=OFFSET(INDEX(Recent!A:A,MATCH(A" + str(row) + ",Recent!A:A,0)),0,2)"
        f3 = "=HYPERLINK(OFFSET(INDEX(Recent!A:A,MATCH(A" + str(row) + ",Recent!A:A,0)),0,3))"
        f4 = "=_xlfn.MINIFS(PriceHistory!C:C,PriceHistory!A:A,A" + str(row) + ")"
        f5 = "=SUMIF(PriceHistory!A:A,A" + str(row) + ", PriceHistory!C:C)/ COUNTIF(PriceHistory!A:A,A" + str(row) + ")"
        f6 = "=_xlfn.MAXIFS(PriceHistory!C:C,PriceHistory!A:A,A" + str(row) + ")"
        rowData = [bucket, f1, f2, f3, f4, f5, f6]
        insert_row(rowData, 0, row)
        row += 1

    delete_range(0, row, get_next_row(0), 7)


# end of get_uniques


# sorts the recent data in order of price
def sort_Recent_data():
    # Recent_data is a list of lists. We need to sort all of these lists by their 3rd item in ascending order
    global Recent_data
    current = 1
    while current < len(Recent_data):
        place_holder = current
        while current > 0 and Recent_data[current][2] < Recent_data[current - 1][2]:
            temp = Recent_data[current - 1]
            Recent_data[current - 1] = Recent_data[current]
            Recent_data[current] = temp
            current -= 1
        current = place_holder + 1


# end of sort_Recent_data


# writes the values from the recent run into the appropriate sheet
def write_Recent():
    row = 2
    for rowData in Recent_data:
        insert_row(rowData, 1, row)
        row += 1
    delete_range(1, row, get_next_row(1), 5)


# end of write_Recent


# Creates a substring of the name from the scraped information
def get_model_from_name(name):
    if name.find('VII') >= 0:
        return 'VII'
    if name.find('Vega 56') >= 0:
        return 'Vega 56'

    index = 0
    model = ''
    while index < len(name) - 2:
        if name[index].isdigit() and name[index + 1].isdigit() and name[index + 2].isdigit():
            model = name[index:name.find(' ', index)]
            if model[len(model) - 2:len(model)] == 'TI':
                return model[0:len(model) - 2] + ' Ti'
            if len(model) > 4:
                index += len(model)
                continue
            else:
                break
        index += 1

    if name.find(' Ti ') >= 0:
        model = model + ' Ti'
    return model


# end of get_model_from_name


# creates a type substring from the scraped information
def get_type_from_name(name):
    name_ = name.lower()  # make search case insensitive
    if "geforce" in name_:
        return 'GeForce'
    if "titan" in name_:
        return 'Titan'
    return 'Radeon'


# end of get_type_from_name


# parses the information at the specified URL
def parse_page_html(url):
    # open connection and grab, then parse the page
    # return false if request doesn't work
    try:
        uClient = urlReq(url)
    except URLError as err:
        print('error in requesting url ', url)
        return False
    page_html = uClient.read()
    uClient.close()
    page_soup = soup(page_html, "html.parser")
    return page_soup


# end of parse_page_html


# designed specificallly to interact with NewEgg's web layout
def scrape_newegg(url):
    global next_row_PriceHistory
    global Recent_data

    page_soup = parse_page_html(url)
    if not page_soup:
        return

    # get all graphic cards on page
    card_Containers = page_soup.findAll("div", {"class": "item-info"})
    for aCard in card_Containers:

        all_a_tags = aCard.findChildren("a")
        if (len(all_a_tags) <= 2):
            continue
        card_url = (all_a_tags[2])["href"]
        card_name = all_a_tags[2].text

        # get card with no ratings
        if not aCard.findChildren('a', title=re.compile('Rating')):
            card_name = all_a_tags[1].text
            card_url = (all_a_tags[1])["href"]

        card_model = get_model_from_name(card_name)
        card_type = get_type_from_name(card_name)
        card_info = card_type + ' ' + card_model  # this is all the info that we'll use to categorize the cards

        # get card price
        price_container = aCard.findAll("li", {"class": "price-current"})
        card_price_text = price_container[0].text
        card_price_text = card_price_text.strip()
        # check card w/o price and skip
        if card_price_text == '':
            continue
        startNum = card_price_text.find('$') + 1
        card_price_text = card_price_text[startNum:card_price_text.find('.', startNum) + 3]
        card_price = float(card_price_text.replace(',', ''))

        # get today's datetime
        d = datetime.datetime.today()
        date = d.strftime('%m/%d/%y')

        # append to PriceHistory
        data = [card_info, card_name, card_price, card_url, date]
        insert_row(data, 2, next_row_PriceHistory)
        next_row_PriceHistory += 1

        # overwrite to Recent
        Recent_data.append(data)
        print(data[1])


# end scrape_newegg


# Designed to scrape from Microcenter's unique web layout
def scrape_microcenter(url):
    global next_row_PriceHistory
    global Recent_data

    page_soup = parse_page_html(url)
    if not page_soup:
        return

    # get all graphic cards on page
    card_Containers = page_soup.findAll('li', {'class': 'product_wrapper'})

    for aCard in card_Containers:
        rightPanel = aCard.findAll('div', {'class': 'result_right'})[0]
        descInfo = rightPanel.div.div.findAll('div', {'class': 'pDescription compressedNormal2'})[0]
        important_info = descInfo.div.h2.a  # <a> tag that has all the important info

        card_url = 'https://www.microcenter.com' + important_info['href']
        card_name = important_info.text
        card_model = get_model_from_name(card_name)
        card_type = get_type_from_name(card_name)
        card_info = card_type + ' ' + card_model  # this is all the info that we'll use to categorize the cards

        # get card price
        card_price_text = important_info['data-price']
        card_price_text = card_price_text.strip()
        # check card w/o price and skip
        if card_price_text == '':
            continue
        card_price = float(card_price_text)

        # get today's datetime
        d = datetime.datetime.today()
        date = d.strftime('%m/%d/%y')

        # append to PriceHistory
        data = [card_info, card_name, card_price, card_url, date]
        insert_row(data, 2, next_row_PriceHistory)
        next_row_PriceHistory += 1

        # overwrite to Recent
        Recent_data.append(data)
        print(data[1])


# end scrape_newegg


# designed to scrape information from Amazon's unique web layout
def scrape_amazon(url):
    global next_row_PriceHistory
    global Recent_data

    page_soup = parse_page_html(url)
    if not page_soup:
        return

    card_Containers = page_soup.findAll("div", {"class": "s-item-container"})

    for aCard in card_Containers:

        # get card_url and name
        card_info_container = aCard.find("a", {
            "class": "a-link-normal s-access-detail-page s-color-twister-title-link a-text-normal"})
        if not card_info_container:
            continue

        card_url = card_info_container['href']
        card_name = card_info_container['title']
        card_model = get_model_from_name(card_name)
        card_type = get_type_from_name(card_name)
        card_info = card_type + ' ' + card_model  # this is all the info that we'll use to categorize the cards

        # get card price
        card_price_container = aCard.find("span", {"class": "a-offscreen"})
        # skip this card if price container doesn't exit, else get price
        if not card_price_container:
            continue
        card_price = card_price_container.text
        card_price = float(card_price[1:len(card_price)].replace(',', ''))

        # get today's datetime
        d = datetime.datetime.today()
        date = d.strftime('%m/%d/%y')

        # append to PriceHistory
        data = [card_info, card_name, card_price, card_url, date]
        insert_row(data, 2, next_row_PriceHistory)
        next_row_PriceHistory += 1

        # overwrite to Recent
        Recent_data.append(data)
        print(data[1])


# end of scrape_amazon


# only executes this code if you run scrapeToExcel.py
# if only parts of this file are called from another file, then the webscraper won't run
if __name__ == "__main__":
    # with our design the scraper can be scaled to access different pages from the currently scraped websites, easily
    filename = 'scrape.xlsx'
    book = load_workbook(filename)
    newegg_url = 'https://www.newegg.com/Video-Cards-Video-Devices/Category/ID-38?Tpk=graphics%20card'
    microcenter_url = 'https://www.microcenter.com/search/search_results.aspx?Ntk=all&sortby=match&N=4294966937&myStore=false'
    amazon_url = 'https://www.amazon.com/Graphics-Cards-Computer-Add-Ons-Computers/b?ie=UTF8&node=284822'

    # next available row in PriceHistory
    next_row_PriceHistory = get_next_row(2)

    # array of most recently scraped graphics cards, each stored as a list
    Recent_data = []

    # scrapes information from each of the specified websites
    scrape_newegg(newegg_url)
    scrape_microcenter(microcenter_url)
    scrape_amazon(amazon_url)

    # if information was scraped
    if len(Recent_data) != 0:
        sort_Recent_data()  # sort Recent_data in ascending order by price
        write_Recent()  # put all newly scraped data into Recent
        get_uniques()  # put only unique values into Results

    book.save(filename)
    startfile(filename)
